# Recipe Book

Sample SPA to test Angular 2+ new features

## Angular Auth

The way authentication works when developing an SPA is by using a token each time a request is sent to the server. Since the objective is to **never** reload the page, the server needs to know we've previously authenticated and when requests are done it searches for the token. Normally, Angular uses **JWT (JSON Web Token)**, but you can use other tokens if you prefer.

### Retrieving token from localStorage and using it in requests

This again depends on whick token and back-end service you're using, but you need to add your authentication in some way to each request you make to the server.

## Angular Modules

* Components, pipes or directives **can't** be declared more than once.
* `CommonModule` contains basic Angular features such like `*ngIf`, `*ngFor`, `ngClass`, etc. It should be commonly added to all modules. On the other hand `BrowserModule` is a module that contains all the features of `CommonModule` plus others that are necessary to launch the app in the browser, so `BrowserModule` **should only be imported in your main module** (usually the `AppModule`).
* Modules imported in the `imports: []` property are imported in the same order you add them. This can be important when sharing your routes through modules in your app. Keep an eye on them because errors can occur due to the order use when importing your modules.
* When using components, pipes or directives across various modules (for example, using it in the app module and in a feature module, or maybe in 2 different feature modules), since you **can't** declare them more than once, the solution is to create a **SharedModule** where you will declare everything you share across your app and then import it everywhere you need.
* If you want to export some Modules that you also need to import, you can just export them. Angular will automatically _import_ them for you
* If a module does not use a component|directive by their selectors, and instead they only use it by routing, they do not need to declare it, but be sure to be precise where and how you specify your components|directives to follow the same convention in your whole app

## Lazy Loading

Everything _imported_ in the `imports: []` array is downloaded by default when loading an Angular app. Therefore, if you _import_ all your modules in your `app.module.ts` you could be downloading unnecessary code.

To Lazy Load features use the `loadChildren: ''` property of routes and specify the file location and class that should be loaded lazily.

### Can Load

What if a route in your lazily loaded module is protected by a guard. It could happen that you load the route and spend performance downloading it and it can't be accessed anyway. To check if a lazily loaded route can be accessed **before** downloading it use the `canLoad` property instead of `canActivate`.

See an example of this in `app-routing.module.ts` and `recipe-routing.module.ts`.

## Service Injection with Lazy Modules

A good practice is to just provide services in the module you're going to use them, but to also remember that, if you provide a service in your `AppModule` and also in any **non-lazy** _FeatureModule_ Angular will use the same instance even though you're _providing_ it 2 times. The only ways Angular will use a new instance of a Service is if you provide it at `Component` level **or** if you _provide_ a service in a lazily loaded Module. If you want to use the same instance for lazily loaded modules you need to add the `providedIn: 'root'` property of the `@Injectable({})` decorator.

## Ahead of time Compilation

This is the process by which Angular compiles our HTML templates into JS -- because accessing JS Code is faster than accessing the DOM -- for performance reasons. Before, people that used Angular would need to ship along with their code Angular's compiler so that their templates would get parsed and then rendered in the browser, but now code can be compilated before the app is shipped to production and downloaded by the browser. That brings multiple benefits:

* Faster Startup
* Templates get check during development shipping, which means earlier error detection
* Smaller file size because compiler no longer needs to be shipped along with your app

## Pre-loading Lazy Loaded Routes

Taking in mind Max's explanation, this feature seems useful for commonly used sections of a WebApp, but not **instantly** used sections. For example, lazy loaded routes are specified to be lazily loaded with the objective of them not being loaded until the user reaches out to them, but if one of those _lazily loaded_ routes is sort of big, when that scenario happens the user might need to wait a little while its loaded. What if while the user uses the eagerly loaded sections we _preload_ some _lazily loaded_ routes.

See an example of this in `app-routing.module.ts`.

## Deploying Angular App

1. Build your app for Prod -> Consider using `ng build --prod --aot` (`--aot` stands for _Ahead of Time Compilation_).
2. Set the correct `<base>` element. Make it targets the correct URL in which your server hosts the app
3. Make sure you **always** return the `index.html` file.

## HttpClient

Key Changes

* You can now use the methods `get()`, `put()`, `post()` and so on as **generic methods** like this -> `get<MyType>()`. Doing this will do 2 main things: First, it will automatically assume you're consulting JSON data and secondly, the arrow function you pass as an argument will assume you'll recieve the type `MyType`.
* You can send an additional optional argument in every request method, which is a JSON where you can configure what you'll receive and how as the response. You can specify you still want to recieve the full response just as the default behaviour of Angular's previous `HttpModule` and many more configurations
* You can know the status of a request which is very useful when uploading or downloading something. For this you need to use Angular's `HttpRequest` object and constructor and set the `reportProgress` value in the configuration object to `true`.
* __Interceptors__, which can do _something_ with every request you specify, like adding an authentication token to every request before sending it. This helps you centralize logic with your requests, reducing repeated code.
  * By default, a request is **immutable** (it can't be modified). You can retry it, but not modifiy it, and this is because if you modify it, and retry it, the request would be modified several times and break at some point. You need to make a **clone** of the request and work with that fresh new copy. See an example in `auth.interceptor.ts`.
* The order by which you add your _interceptors_ in the `providers: []` array is the order in which they'll get executed.
