import { NgModule } from '@angular/core';
import { DropdownDirective } from '../directives/dropdown.directive';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    DropdownDirective
  ],
  exports: [CommonModule, DropdownDirective, ReactiveFormsModule]
})
export class SharedModule {}
