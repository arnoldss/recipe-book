import { NgModule } from '@angular/core';
import { SigninComponent } from '../components/signin/signin.component';
import { SignupComponent } from '../components/signup/signup.component';
import { SharedModule } from './shared.module';
import { AuthRoutingModule } from './routing/auth-routing.module';

@NgModule({
  declarations: [
    SignupComponent,
    SigninComponent
  ],
  imports: [AuthRoutingModule, SharedModule]
})
export class AuthModule {}
