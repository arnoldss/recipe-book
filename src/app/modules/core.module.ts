import { NgModule } from '@angular/core';
import { HeaderComponent } from '../components/header/header.component';
import { HomeComponent } from '../components/home/home.component';
import { SharedModule } from './shared.module';
import { AppRoutingModule } from './routing/app-routing.module';

@NgModule({
  declarations: [HeaderComponent, HomeComponent],
  imports: [AppRoutingModule, SharedModule],
  exports: [AppRoutingModule, HeaderComponent]
})
export class CoreModule {}
