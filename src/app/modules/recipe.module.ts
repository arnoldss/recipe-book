import { NgModule } from '@angular/core';
import { RecipesComponent } from '../components/recipes/recipes.component';
import { RecipeListComponent } from '../components/recipe-list/recipe-list.component';
import { RecipeDetailComponent } from '../components/recipe-detail/recipe-detail.component';
import { RecipeUnselectedComponent } from '../components/recipe-unselected/recipe-unselected.component';
import { RecipeEditComponent } from '../components/recipe-edit/recipe-edit.component';
import { RecipeItemComponent } from '../components/recipe-item/recipe-item.component';
import { RecipeRoutingModule } from './routing/recipe-routing.module';
import { SharedModule } from './shared.module';

@NgModule({
  declarations: [
    RecipesComponent,
    RecipeListComponent,
    RecipeDetailComponent,
    RecipeUnselectedComponent,
    RecipeEditComponent,
    RecipeItemComponent
  ],
  imports: [SharedModule, RecipeRoutingModule]
})
export class RecipeModule {}
