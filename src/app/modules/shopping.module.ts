import { NgModule } from '@angular/core';
import { ShoppingListComponent } from '../components/shopping-list/shopping-list.component';
import { ShoppingListEditComponent } from '../components/shopping-list-edit/shopping-list-edit.component';
import { SharedModule } from './shared.module';
import { ShoppingRoutingModule } from './routing/shopping-routing.module';

@NgModule({
  declarations: [
    ShoppingListComponent,
    ShoppingListEditComponent
  ],
  imports: [SharedModule, ShoppingRoutingModule]
})
export class ShoppingModule {}
