import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RecipesComponent } from 'src/app/components/recipes/recipes.component';
import { RecipeUnselectedComponent } from 'src/app/components/recipe-unselected/recipe-unselected.component';
import { RecipeEditComponent } from 'src/app/components/recipe-edit/recipe-edit.component';
import { AuthGuard } from 'src/app/guards/auth.guard';
import { RecipeDetailComponent } from 'src/app/components/recipe-detail/recipe-detail.component';

const routes: Routes = [
  {
    path: '',
    component: RecipesComponent,
    children: [
      { path: '', component: RecipeUnselectedComponent },
      { path: 'new', component: RecipeEditComponent, canActivate: [AuthGuard] },
      {
        path: ':id',
        component: RecipeDetailComponent,
        canActivate: [AuthGuard]
      },
      {
        path: ':id/edit',
        component: RecipeEditComponent,
        canActivate: [AuthGuard]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RecipeRoutingModule {}
