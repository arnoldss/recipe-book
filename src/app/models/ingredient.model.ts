export class Ingredient {
  private static id = 0;
  public id: number;

  constructor(public name: string, public amount: number) {
    this.id = Ingredient.id++;
  }
}
