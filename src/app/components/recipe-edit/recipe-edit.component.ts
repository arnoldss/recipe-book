import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import {
  FormGroup,
  FormControl,
  FormArray,
  AbstractControl,
  Validators
} from '@angular/forms';
import { RecipeService } from 'src/app/services/recipe.service';
import { Recipe } from 'src/app/models/recipe.model';

@Component({
  selector: 'app-recipe-edit',
  templateUrl: './recipe-edit.component.html',
  styleUrls: ['./recipe-edit.component.css']
})
export class RecipeEditComponent implements OnInit, OnDestroy {
  recipeForm: FormGroup;
  editingRecipeId: number;
  editMode = false;
  paramsSub: Subscription;

  constructor(
    private router: Router,
    private currentRoute: ActivatedRoute,
    private recipeService: RecipeService
  ) {}

  ngOnInit() {
    this.paramsSub = this.currentRoute.params.subscribe((params: Params) => {
      this.editingRecipeId = +params['id'];
      // If params doesn't contain dynamic parameter 'id' then we're creating a new recipe
      this.editMode = params['id'] != null;
      this.initForm();
    });
  }

  ngOnDestroy() {
    this.paramsSub.unsubscribe();
  }

  private initForm(): void {
    let recipeName = '';
    let recipeImagePath = '';
    let recipeDescription = '';
    let recipeIngredients = new FormArray([]);

    if (this.editMode) {
      const editingRecipe = this.recipeService.getRecipeById(
        this.editingRecipeId
      );
      recipeName = editingRecipe.name;
      recipeImagePath = editingRecipe.imagePath;
      recipeDescription = editingRecipe.description;

      if (editingRecipe['ingredients'] != null) {
        for (const ingredient of editingRecipe.ingredients) {
          recipeIngredients.push(
            new FormGroup({
              name: new FormControl(ingredient.name, Validators.required),
              amount: new FormControl(ingredient.amount, [
                Validators.required,
                Validators.pattern('^[1-9]+[0-9]*$')
              ])
            })
          );
        }
      }
    }

    this.recipeForm = new FormGroup({
      name: new FormControl(recipeName, Validators.required),
      imagePath: new FormControl(recipeImagePath, Validators.required),
      description: new FormControl(recipeDescription, Validators.required),
      ingredients: recipeIngredients
    });
  }

  getControls(formArray): AbstractControl[] {
    return formArray.controls;
  }

  onAddIngredient(): void {
    (<FormArray>this.recipeForm.get('ingredients')).push(
      new FormGroup({
        name: new FormControl(null, Validators.required),
        amount: new FormControl(null, [
          Validators.required,
          Validators.pattern('^[1-9]+[0-9]*$')
        ])
      })
    );
  }

  onDeleteIngredient(index: number): void {
    (<FormArray>this.recipeForm.get('ingredients')).removeAt(index);
  }

  onCancel(): void {
    this.router.navigate(['../'], { relativeTo: this.currentRoute });
  }

  onSubmit(): void {
    const name = this.recipeForm.value['name'];
    const imagePath = this.recipeForm.value['imagePath'];
    const description = this.recipeForm.value['description'];
    const ingredients = this.recipeForm.value['ingredients'];
    const newRecipe = new Recipe(name, description, imagePath, ingredients);
    if (this.editMode) {
      newRecipe.id = this.editingRecipeId;
      this.recipeService.updateRecipe(this.editingRecipeId, newRecipe);
      this.editMode = false;
    } else {
      this.recipeService.addRecipe(newRecipe);
    }
    this.onCancel();
  }
}
