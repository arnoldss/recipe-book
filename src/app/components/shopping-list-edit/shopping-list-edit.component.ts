import { Component, OnInit, OnDestroy } from '@angular/core';
import { Ingredient } from '../../models/ingredient.model';
import { ShoppingService } from 'src/app/services/shopping.service';
import {
  FormGroup,
  FormBuilder,
  Validators,
  ValidationErrors,
  FormControl
} from '@angular/forms';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-shopping-list-edit',
  templateUrl: './shopping-list-edit.component.html'
})
export class ShoppingListEditComponent implements OnInit, OnDestroy {
  ingredientForm: FormGroup;
  editSub: Subscription;
  editMode: boolean;
  editIngredientId: number;

  constructor(private shoppingService: ShoppingService) {}

  ngOnInit() {
    this.editMode = false;
    this.editSub = this.shoppingService.startedEditing.subscribe(
      (ingredient: Ingredient) => {
        this.editMode = true;
        this.editIngredientId = ingredient.id;
        this.ingredientForm.setValue({
          ingredientName: ingredient.name,
          amount: ingredient.amount
        });
      }
    );
    this.ingredientForm = new FormGroup({
      ingredientName: new FormControl(null, Validators.required),
      amount: new FormControl(null, [
        Validators.required,
        Validators.pattern('^[1-9]+[0-9]*$')
      ])
    });
  }

  ngOnDestroy() {
    this.editSub.unsubscribe();
  }

  onSubmit(): void {
    const ingredientName = this.ingredientForm.get('ingredientName').value;
    const ingredientAmount = +this.ingredientForm.get('amount').value;
    if (this.editMode) {
      this.shoppingService.updateIngredient(this.editIngredientId, ingredientName, ingredientAmount);
      this.editMode = false;
    } else {
      this.shoppingService.addIngredient(
        new Ingredient(ingredientName, ingredientAmount)
      );
    }
    this.ingredientForm.reset();
  }

  onDeleteIngredient(): void {
    this.shoppingService.deleteIngredient(this.editIngredientId);
    this.ingredientForm.reset();
    this.editMode = false;
  }
}
