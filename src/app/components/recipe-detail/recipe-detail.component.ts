import { Component, OnInit, OnDestroy } from '@angular/core';
import { Recipe } from 'src/app/models/recipe.model';
import { ShoppingService } from 'src/app/services/shopping.service';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { RecipeService } from 'src/app/services/recipe.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-recipe-detail',
  templateUrl: './recipe-detail.component.html',
  styleUrls: ['./recipe-detail.component.css']
})
export class RecipeDetailComponent implements OnInit, OnDestroy {
  recipe: Recipe;
  paramsSub: Subscription;
  recipesChangedSub: Subscription;

  constructor(
    private recipeService: RecipeService,
    private shoppingService: ShoppingService,
    private router: Router,
    private currentRoute: ActivatedRoute
  ) {}

  ngOnInit() {
    this.paramsSub = this.currentRoute.params.subscribe((params: Params) => {
      this.recipe = this.recipeService.getRecipeById(+params['id']);
    });
    this.recipesChangedSub = this.recipeService.recipesChanges.subscribe(
      (recipes: Recipe[]) => this.recipe = this.recipeService.getRecipeById(this.recipe.id)
    );
  }

  ngOnDestroy() {
    this.paramsSub.unsubscribe();
    this.recipesChangedSub.unsubscribe();
  }

  onAddToShoppingList(): void {
    this.shoppingService.addIngredients(this.recipe.ingredients);
  }

  onDeleteRecipe(): void {
    this.recipeService.deleteRecipe(this.recipe.id);
    this.router.navigate(['recipes']);
  }
}
