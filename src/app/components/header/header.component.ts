import { Component, OnInit } from '@angular/core';
import { DataStorageService } from 'src/app/services/data-storage.service';
import { Response } from '@angular/http';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html'
})
export class HeaderComponent implements OnInit {
  microApps: { name: string; }[] = environment.microApps;

  constructor(
    private dsService: DataStorageService,
    private auth: AuthService,
    private router: Router
  ) {}

  ngOnInit() {}

  isAuthenticated(): boolean {
    return this.auth.isAuthenticated();
  }

  onFetch(): void {
    this.dsService.getRecipes();
  }

  onSave(): void {
    this.dsService
      .storeRecipes()
      .subscribe((response: Response) => console.log(response));
  }

  onSignOut(): void {
    this.auth.signOut();
    this.router.navigate(['/recipes']);
  }
}
