import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-recipe-unselected',
  templateUrl: './recipe-unselected.component.html',
  styleUrls: ['./recipe-unselected.component.css']
})
export class RecipeUnselectedComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
