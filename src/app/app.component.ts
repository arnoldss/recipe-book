import { Component, OnInit } from '@angular/core';
import * as firebase from 'firebase';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  constructor() {}

  ngOnInit() {
    firebase.initializeApp({
      apiKey: 'AIzaSyAlHqc8PDmzmOT1OHZ1DqggzlQ-gTgp1gM',
      authDomain: 'recipe-book-c6742.firebaseapp.com'
    });
  }
}
