import { Directive, Renderer2, HostListener, ElementRef } from '@angular/core';

@Directive({
  selector: '[appDropdown]'
})
export class DropdownDirective {
  isExpanded: boolean;

  constructor(private renderer: Renderer2, private elementRef: ElementRef) {
    this.isExpanded = false;
  }

  @HostListener('click') onClick() {
    const dropdownDiv = this.elementRef.nativeElement.querySelector(
      'div.dropdown-menu'
    );
    if (!this.isExpanded) {
      this.renderer.addClass(dropdownDiv, 'show');
    } else {
      this.renderer.removeClass(dropdownDiv, 'show');
    }
    this.isExpanded = !this.isExpanded;
  }
}
