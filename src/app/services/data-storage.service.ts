import { Injectable } from '@angular/core';
import { Response } from '@angular/http';
import { RecipeService } from './recipe.service';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Recipe } from '../models/recipe.model';
import { AuthService } from './auth.service';
import { HttpClient, HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DataStorageService {
  constructor(
    private httpClient: HttpClient,
    private recipeService: RecipeService,
    private auth: AuthService
  ) {}

  getRecipes() {
    return this.httpClient
      .get<Recipe[]>('https://recipe-book-c6742.firebaseio.com/recipes.json')
      .pipe(
        map(recipes => {
          for (const recipe of recipes) {
            if (!recipe['ingredients']) {
              recipe['ingredients'] = [];
            }
          }
          return recipes;
        })
      )
      .subscribe((recipes: Recipe[]) => {
        this.recipeService.setRecipes(recipes);
      });
  }

  storeRecipes() {
    return this.httpClient.put(
      'https://recipe-book-c6742.firebaseio.com/recipes.json',
      this.recipeService.getRecipes(),
      {
        observe: 'body'
      }
    );
  }
}
