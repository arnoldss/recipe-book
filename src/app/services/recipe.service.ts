import { Recipe } from '../models/recipe.model';
import { Injectable, Output, EventEmitter } from '@angular/core';
import { Ingredient } from '../models/ingredient.model';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RecipeService {
  public recipesChanges = new Subject<Recipe[]>();
  private recipes: Recipe[];

  constructor() {
    this.recipes = [];
  }

  addRecipe(recipe: Recipe): void {
    this.recipes.push(recipe);
    this.recipesChanges.next(this.getRecipes());
  }

  deleteRecipe(id: number) {
    const recipeToRemove = this.recipes.find((recipe: Recipe) => recipe.id === id);
    const index = this.recipes.indexOf(recipeToRemove);
    this.recipes.splice(index, 1);
    this.recipesChanges.next(this.getRecipes());
  }

  getRecipeById(id: number): Recipe {
    return this.getRecipes().find((recipe: Recipe) => recipe.id === id);
  }

  getRecipes(): Recipe[] {
    return this.recipes.slice();
  }

  setRecipes(recipes: Recipe[]) {
    this.recipes = recipes;
    this.recipesChanges.next(this.getRecipes());
  }

  updateRecipe(id: number, newRecipe: Recipe): void {
    const recipeToUpdate = this.recipes.find(
      (recipe: Recipe) => recipe.id === id
    );
    const index = this.recipes.indexOf(recipeToUpdate);
    this.recipes[index] = newRecipe;
    this.recipesChanges.next(this.getRecipes());
  }
}
