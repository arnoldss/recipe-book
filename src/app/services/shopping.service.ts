import { Ingredient } from '../models/ingredient.model';
import { Injectable, Output, EventEmitter } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ShoppingService {
  ingredientsChanged = new Subject<Ingredient[]>();
  startedEditing = new Subject<Ingredient>();
  private ingredients: Ingredient[];

  constructor() {
    this.ingredients = [];
  }

  getIngredients(): Ingredient[] {
    return this.ingredients.slice();
  }

  addIngredient(ingredient: Ingredient): void {
    this.ingredients.push(ingredient);
    this.ingredientsChanged.next(this.getIngredients());
  }

  addIngredients(ingredients: Ingredient[]): void {
    this.ingredients.push(...ingredients);
    this.ingredientsChanged.next(this.getIngredients());
  }

  deleteIngredient(id: number): void {
    const ingredientToRemove = this.ingredients.find(
      (ingredient: Ingredient) => ingredient.id === id
    );
    this.ingredients.splice(this.ingredients.indexOf(ingredientToRemove), 1);
    this.ingredientsChanged.next(this.getIngredients());
  }

  updateIngredient(id: number, name: string, amount: number) {
    const ingredientToUpdate = this.ingredients.find(
      (ingredient: Ingredient) => ingredient.id === id
    );
    const index = this.ingredients.indexOf(ingredientToUpdate);
    this.ingredients[index] = new Ingredient(name, amount);
    this.ingredientsChanged.next(this.getIngredients());
  }
}
